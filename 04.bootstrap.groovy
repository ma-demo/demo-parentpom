def flow
node{
  echo "bootstrap function begin"
  echo "pwd: " + pwd()
  git url: 'https://bitbucket.org/mindapproach/demo-parentpom.git'
  flow = load "04.flow.groovy"
  echo "bootstrap function end"
}
flow.internalBuild()
